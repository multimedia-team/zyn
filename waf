#! /usr/bin/env python
# encoding: utf-8
# Thomas Nagy, 2005-2008

"""
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
"""

import os, sys
if sys.hexversion<0x203000f: raise "Waf requires Python >= 2.3"

if 'PSYCOWAF' in os.environ:
	try:import psyco;psyco.full()
	except:pass

VERSION="1.3.3"
REVISION="6ab4e266aab677a9c822fc0e23df0098"
INSTALL=sys.platform=='win32' and 'c:/temp' or '/usr/local'
cwd = os.getcwd()
join = os.path.join

def decode(s):
	p1 = len(s)
	s += '!!!!!'
	w1 = [256**(3-u) for u in xrange(4)]
	w2 = [(u, 85**(4-u)) for u in xrange(5)]
	tot = [sum([(ord(s[i+m])-33) * n for (m, n) in w2]) for i in xrange(0, p1, 5)]
	return ''.join([chr((y/x) & 255) for y in tot for x in w1])

def err(m):
	print '\033[91mError: %s\033[0m' % m
	sys.exit(1)

def unpack_wafdir(dir):
	f = open(sys.argv[0],'rb')
	c = "corrupted waf (%d)"
	while 1:
		line = f.readline()
		if not line: err("run waf-light from a folder containing wafadmin")
		if line == '#==>\n':
			txt = f.readline()
			if not txt: err(c % 1)
			if f.readline()!='#<==\n': err(c % 2)
			break
	if not txt: err(c % 3)
	try: txt = decode(txt[1:-1].replace('z', '!!!!!'))
	except: err(c % 4)

	import shutil, tarfile
	try: shutil.rmtree(dir)
	except OSError: pass
	try: os.makedirs(join(dir, 'wafadmin', 'Tools'))
	except OSError: err("Cannot unpack waf-local into %s Install waf system-wide or move waf into a writeable directory"%dir)

	os.chdir(dir)
	tmp = 't.tbz2'
	t = open(tmp,'wb')
	t.write(txt)
	t.close()

	t = tarfile.open(tmp)
	for x in t: t.extract(x)
	t.close()

	os.unlink(tmp)
	os.chdir(cwd)

def test(dir):
	try: os.stat(join(dir, 'wafadmin')); return os.path.abspath(dir)
	except OSError: pass

def find_lib():
	name = sys.argv[0]
	base = os.path.dirname(os.path.abspath(name))

	#devs use $WAFDIR
	w=test(os.environ.get('WAFDIR', ''))
	if w: return w

	#waf-light
	if name.endswith('waf-light'):
		w = test(base)
		if w: return w
		err("waf-light requires wafadmin -> export WAFDIR=/folder")

	dir = "/lib/waf-%s-%s/" % (VERSION, REVISION)
	for i in [INSTALL,'/usr','/usr/local','/opt']:
		w = test(i+dir)
		if w: return w

	#waf-local
	s = '.waf-%s-%s'
	if sys.platform == 'win32': s = s[1:]
	dir = join(base, s % (VERSION, REVISION))
	w = test(dir)
	if w: return w

	#unpack
	unpack_wafdir(dir)
	return dir

wafdir = find_lib()
if "-vv" in sys.argv: print "wafdir is %s" % wafdir

w = join(wafdir, 'wafadmin')
t = join(w, 'Tools')
sys.path = [w, t] + sys.path

import Params, Scripting
Params.g_tooldir = [t]
Params.g_cwd_launch = cwd

if Params.g_version != VERSION:
	err('Version mismatch: waf %s <> wafadmin %s (wafdir %s)' % (VERSION, Params.g_version, wafdir))
Scripting.prepare()

